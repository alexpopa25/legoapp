import React from 'react';
import { StyleSheet, Text, View, Image} from 'react-native';


interface IProps {
    title: string,
    logo_url: string
}



export default class Header extends React.Component<IProps>{
   
    render(){
        const {title, logo_url} = this.props;
        return (
            <View style={styles.viewStyles}>
                <Image source={{uri: logo_url}} style={styles.logoStyles}/>
                <Text style={styles.textStyles}>{title}</Text>
            </View>
        )
    }

}

const styles = StyleSheet.create({
    viewStyles: {
       backgroundColor: "#FFE330",
       flexDirection: "row",
       justifyContent: "space-between",
       alignItems: "center",
       height: 70,
       shadowColor: "black",
       shadowOffset: {width: 0, height: 2},
       shadowOpacity: 0.2,
       elevation: 3
      
    },
    textStyles: {
      color: "black",
      marginRight: 160
    },
    logoStyles:{
       width: 60,
       height: 60
    }
})