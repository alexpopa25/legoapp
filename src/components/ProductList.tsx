import React from 'react';
import {createRef} from 'react';
import { StyleSheet, Text, View, Image, FlatList, TouchableOpacity} from 'react-native';

import { connect } from 'react-redux';
import {fetchProductsFromApi} from './../redux/actions'
import  ProductListItem  from './ProductListItem';
import AddProductModal from './AddProductModal';
import {NavigationScreenProps} from 'react-navigation'




interface IProps {
  getProducts: any;
  products: any
  navigation: Navigator
}

class ProductList extends React.Component<IProps>{
  
  
    componentDidMount(){
       
        this.props.getProducts()
       
    }
    
 
 
    
    render(){
        const { products, isFetching }  = this.props.products;
        console.log('products: ', products.d)
        // Object.keys(products).length
        return (
            <View>
           
                    <FlatList
                     data={products.d}
                     keyExtractor={(item, index) => index.toString()}
                     renderItem={({item, index})=>{
                       return (
                           <ProductListItem item={item} index={index}>
                           </ProductListItem>
                       );
                     }}
                     
                     >
                    
                    </FlatList>   
                    <View style={styles.btnContainer}>
                    <TouchableOpacity style={styles.btn} >
                        <Text style={styles.btnText}>+</Text>
                    </TouchableOpacity> 
                    </View>
                    
                  
            </View>
               
        
            
        )
    }

}

const styles = StyleSheet.create({
  viewStyle: {
      flex: 1
  },
  btnContainer: {
      position: "absolute",
      width: 70,
      height:70,
      right: 30,
      bottom: 170,
   
  },
  btn: {
     
      width: 70,
      height:70,
      backgroundColor: "red",
      borderRadius: 50,
      alignItems: "center",
      justifyContent: "center",
     
  },
  btnText: {
      color: "white",
      fontSize: 25
  }
})

function mapStateToProps(state: any){

  return {
      products: state.products
  }
}

function mapDispatchToProps(dispatch: any){
  return {
      getProducts: () => dispatch(fetchProductsFromApi())
  }
}

export default connect(mapStateToProps, mapDispatchToProps)(ProductList)