/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */

import React from 'react'
import { Component } from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import ProductList from '../components/ProductList';
import {createStackNavigator} from 'react-navigation';


import Header from '../components/Header'

  
type Props = {};
export default class ProductListScreen extends Component<Props> {
  render() {
    return (
      <View >
        <Header title="My Products" logo_url="https://s3.amazonaws.com/freebiesupply/thumbs/2x/lego-logo.png" />
        <ProductList />
        
      </View>
   
    );
  }
}

const styles = StyleSheet.create({

});
