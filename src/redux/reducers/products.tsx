import {FETCHING_PRODUCTS,FETCHING_PRODUCTS_SUCCESS,FETCHING_PRODUCTS_FAILURE } from '../constants';

const initialState = {
    products: {},
    isFetching: false,
    error: false
}

export default function productsReducer(state = initialState, action: any){
    switch(action.type){
        case FETCHING_PRODUCTS: 
            return {
                ...state,
                isFetching: true,
                products: {}
            }
        case FETCHING_PRODUCTS_SUCCESS:
            return {
                ...state,
                isFetching: false,
                products: action.data
            }
        case FETCHING_PRODUCTS_FAILURE:
            return {
                ...state,
                isFetching: false,
                error: true
            }
        default: 
            return state;    
    }
}