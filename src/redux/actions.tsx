import {FETCHING_PRODUCTS,FETCHING_PRODUCTS_SUCCESS,FETCHING_PRODUCTS_FAILURE } from './constants';


export function fetchProductsFromApi(){
    return (dispatch: any) => {
        dispatch(getProducts())
        fetch('https://services.odata.org/V2/(S(1pvfcxajki3ouml43k5wnex3))/OData/OData.svc/Products', {
            headers: {
                "Accept": "application/json"
            }
        })
              .then((response)=>{
                  return response.json()
              })
              .then((response)=>{
                dispatch(getProductsSuccess(response))
              })
              .catch(err => dispatch(getProductsFailure(err)))
    }
}

function getProducts(){
  return {
      type: FETCHING_PRODUCTS
  }
}

function getProductsSuccess(data: any){
    // console.log(data)
    return {
        type: FETCHING_PRODUCTS_SUCCESS,
        data
    }
  }

  function getProductsFailure(err: any){
    return {
        type: FETCHING_PRODUCTS_FAILURE
    
    }
  }