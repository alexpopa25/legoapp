How to run the app
==================

Prerequisites:
1. Android Studio with AVD;
2. Java JDK;
2. React-Native;

To run the app:
1. Open a terminal in the root folder and run:
`npm install`
2. Open an android virtual device from Android Studio;
3. Open a terminal in the root folder of the application and run:
 `react-native run-android`
4. The application should start in the virtual device;


Considerations:
1. Switched from Mobx State Tree to React-Redux for the state management;
2. Started from scratch and installed typescript manually following this guide:
[Guide for using typescript with react-native](https://facebook.github.io/react-native/blog/2018/05/07/using-typescript-with-react-native)
3. Was unable to finish all the functionality, mainly adding new products;
